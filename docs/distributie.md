# Distributie

Op mijn home server gebruik ik Arch Linux. Hier zitten een aantal voor- en nadelen aan. 

Voordelen:
- rolling release; nooit een nieuwe distributie installeren
- goede wiki's

Nadelen:
- snel nieuwe updates die soms nog bugs bevatten

Als ik mijn home server opnieuw zou installeren zou ik wellicht Debian overwegen. Debian staat bekend als een zeer stabiele distributie. 

Voordelen:
- stabiel
- veel documentatie

Nadelen:
- het duurt lang voordat er updates zijn
- je zult op een gegeven moment een distributie update moeten doen

De afgelopen periode heb ik gelezen dat Ubuntu server ten opzichte van Debian geen meerwaarde heeft. Het is niet mijn mening, ik geef hier alleen weer wat ik ergens gelezen heb.
