# Hardware

De keuze in processor architecteur is helaas ook weer bepalend voor de keuze in distributies. Voor de 'lichte' server valt daarom wat mij betreft een ARM processor af, omdat het aantal Linux distributies daarvoor beperkt is. Een Raspberry Pi valt bij mij ook om een andere reden af: Ik heb een Raspberry Pi (1) een tijdje als fileservertje 24/7 aangehad en na verloop van tijd crashte deze.

Het lijkt mij het meest praktisch als voor zowel de 'lichte' als de 'zware' server gebruik gemaakt kan worden van dezelfde Linux distributie.